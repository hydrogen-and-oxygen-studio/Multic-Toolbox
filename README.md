# Multic-Toolbox
An intelligent toolbox.

## What can it do?
* Multiple clipboard
* System info
* More features are under development. Please be patient.

## Before use
```
bash
cd ~
pip3 install shelve pyperclip psutil uuid socket --user
```
## How to use
View the introduction files for each tool to find out how to use it.
## LICENSE
MIT

###### Copyright (c) 2019 Hydrogen and Oxygen Studio